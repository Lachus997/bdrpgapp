import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ProstaPostac {
    private SimpleIntegerProperty id;
    private SimpleStringProperty  imie;
    private SimpleStringProperty  nazwisko_pseudonim;
    private SimpleStringProperty  rasa;
    private SimpleStringProperty  klasa;
    private SimpleIntegerProperty poziom;

    public ProstaPostac() {
        id = new SimpleIntegerProperty();
        imie = new SimpleStringProperty();
        nazwisko_pseudonim = new SimpleStringProperty();
        rasa = new SimpleStringProperty();
        klasa = new SimpleStringProperty();
        poziom = new SimpleIntegerProperty();
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getImie() {
        return imie.get();
    }

    public SimpleStringProperty imieProperty() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie.set(imie);
    }

    public String getNazwisko_pseudonim() {
        return nazwisko_pseudonim.get();
    }

    public SimpleStringProperty nazwisko_pseudonimProperty() {
        return nazwisko_pseudonim;
    }

    public void setNazwisko_pseudonim(String nazwisko_pseudonim) {
        this.nazwisko_pseudonim.set(nazwisko_pseudonim);
    }

    public String getRasa() {
        return rasa.get();
    }

    public SimpleStringProperty rasaProperty() {
        return rasa;
    }

    public void setRasa(String rasa) {
        this.rasa.set(rasa);
    }

    public String getKlasa() {
        return klasa.get();
    }

    public SimpleStringProperty klasaProperty() {
        return klasa;
    }

    public void setKlasa(String klasa) {
        this.klasa.set(klasa);
    }

    public int getPoziom() {
        return poziom.get();
    }

    public SimpleIntegerProperty poziomProperty() {
        return poziom;
    }

    public void setPoziom(int poziom) {
        this.poziom.set(poziom);
    }

    public String getValue(String columnname){
        if (columnname.equalsIgnoreCase("imie")){
            return imie.getValue();
        } else if (columnname.equalsIgnoreCase("nazwisko_pseudonim")){
            return nazwisko_pseudonim.getValue();
        } else if (columnname.equalsIgnoreCase("rasa")){
            return rasa.getValue();
        } else if (columnname.equalsIgnoreCase("klasa")){
            return klasa.getValue();
        } else if (columnname.equalsIgnoreCase("poziom")){
            return poziom.getValue().toString();
        } else {
            return null;
        }
    }
}
