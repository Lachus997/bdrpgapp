import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;


/**
 * Kontroler pierwszego wykożystywanego przez nas okna: zawiera 2 meody, każda z nich przenosi nas do okna logowania dla innego rodzaju placówki.
 */
public class KontrolerOknaWyboruApki {

    @FXML
    private Button MistrzButtom;

    @FXML
    private Button GraczButton;

    /**
     * @param event
     * @throws IOException Pierwsza metoda sluzy do otwarcia okna logowania dla banku krwi, ponownie nie ma tu czego opisywać. Metoda ta podobnie jak metoda z poprzedniej klasy jak i nastepna metoda wykozystuje odnosnik do pliku fxml
     */
    @FXML
    void openMenuMistrza(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoLogowaniaMistrza.fxml");
        loader.setLocation(fxmlUrl);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) MistrzButtom.getScene().getWindow();
        stageteraz.close();
    }

    /**
     * @param event
     * @throws IOException Analogicznie do metody OpenBank metoda ta za pomocą odniesienia do pliku fxml otwiera okno sluzace do zalogowania sie do aplikacji do obslugi centrum krwiodastwa
     */
    @FXML
    void openMenuGracza(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoLogowaniaGracza.fxml");
        loader.setLocation(fxmlUrl);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) GraczButton.getScene().getWindow();
        stageteraz.close();
    }

    @FXML
    public void initialize() {

    }

}
