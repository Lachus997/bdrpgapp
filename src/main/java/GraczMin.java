import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class GraczMin {
    private SimpleIntegerProperty id;
    private SimpleStringProperty login;

    public GraczMin() {
        id = new SimpleIntegerProperty();
        login = new SimpleStringProperty();
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getLogin() {
        return login.get();
    }

    public SimpleStringProperty loginProperty() {
        return login;
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public String getValue(String columnName) {
        return "A";
    }
    public Integer getValueInt(String columnName) {
        return 1;
    }

}
