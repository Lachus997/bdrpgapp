import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna pozwalajacego na wybor id probki do transfuzji (usuniecia z tabeli jednostek)
 */
public class KontrolerOknaSzczegolowKampanii2 {

    @FXML
    private Button Wroc;

    @FXML
    private Label tytulField;

    @FXML
    private Label settingField;

    @FXML
    private Label maxField;

    @FXML
    private Label opisField;

    @FXML
    private Label blad;

    private int id;

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    public void Start() throws SQLException {

        Connection conn = null;

        try{
            conn = DriverManager.getConnection(connectionUrl);
        } catch (Exception e){
            e.printStackTrace();
        }

        try{
            PreparedStatement statement = conn.prepareStatement("select * from kampania where id = ?");
            statement.setInt(1,id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            tytulField.setText("Tytul: " + resultSet.getString("tytul"));
            settingField.setText("Setting: " + resultSet.getString("setting"));
            maxField.setText("Max graczy: " + String.valueOf(resultSet.getInt("max_graczy")));
            opisField.setText("Opis: " + resultSet.getString("opis"));
        } catch (Exception e){
            e.printStackTrace();
            blad.setText("Cos poszlo nie tak przy ladowaniu danych");
        }
        conn.close();
    }

    /**
     * @param event
     * @throws IOException
     * Powrot do okna pozwalajacego na wybor funkcji
     */
        @FXML
    void Powrot(ActionEvent event) throws IOException, SQLException {
            FXMLLoader loader = new FXMLLoader();
            URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoKampaniGracza.fxml");
            loader.setLocation(fxmlUrl);
            loader.load();
            KontrolerOknaKampaniGracza controler = loader.getController();
            controler.setConnectionUrl(connectionUrl);
            controler.setLogin(login);
            controler.wypelnij();
            Parent root = loader.getRoot();
            Scene scene = new Scene(root);
            Stage primaryStage = new Stage();
            primaryStage.setScene(scene);
            primaryStage.setTitle("ProjektBDLog");
            primaryStage.show();

            Stage stageteraz = (Stage) Wroc.getScene().getWindow();
            stageteraz.close();
    }


    public void setid(int id) {
            this.id = id;
    }

}
