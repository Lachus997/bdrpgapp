import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna sluzacego do wyswietlania widoku zapasy w przygotowanej tabeli
 */
public class KontrolerOknaSzukaniaKampanii {

    @FXML
    private Button Wroc;

    @FXML
    private TableView<WolneKampanie> tabelka;

    @FXML
    private TextField szukanyTytulField;

    @FXML
    private TextField szukanySettingField;

    @FXML
    private Button szukaj;

    private ObservableList<WolneKampanie> wolneKampanies;

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    /**
     * @param event
     * @throws IOException Ponowne otworzenie okna wyboru funkcji i zamkniecie otwartego okna
     */
    @FXML
    void Powrot(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoMenuGracza.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaMenuGracza controler = loader.getController();
        controler.setLogin(login);
        controler.setConnectionUrl(connectionUrl);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }

    /**
     * @throws SQLException Metoda ta pozwala na nawiazanie polaczenia, wywolacnie select all na widoku zapasy i wyswietlenie jego zawartosci w tabeli
     *                      W funkcji tej najpierw wszystkie kolumnt tabeli sa przygotowane na "odbior danych" wszystkie dane zawarte w widoku sa przetwarzane
     *                      tak, aby mozna je bylo zobrazowac w tabeli
     */
    @FXML
    public void wypelnij() throws SQLException {
        String zap = "Select * from wolne_kampanie where id not In(select  kampania_id from gracz_kampania where gracz_id = (select id from gracz where login = ? ))";

        try {
            Connection conn = DriverManager.getConnection(connectionUrl);
            PreparedStatement statement = conn.prepareStatement(zap);
            statement.setString(1,login);
            ResultSet rs = statement.executeQuery();

            tableFilling(rs);

            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    @FXML
    public void szukaj() throws SQLException {
        String zap = "Select * from wolne_kampanie where id!=(select  kampania_id from gracz_kampania where gracz_id = (select id from gracz where login = ? ))";
        System.out.println(zap);

        try {
            int miej = 2;
            Connection conn = DriverManager.getConnection(connectionUrl);

            if (!szukanyTytulField.getText().equals("")){
                zap = zap + "and tytul = ?";
            }
            if(!szukanySettingField.getText().equals("")){
                zap = zap = "and setting = ?";
            }

            PreparedStatement statement = conn.prepareStatement(zap);
            statement.setString(1,login);

            if (!szukanyTytulField.getText().equals("")){
                statement.setString(2,szukanyTytulField.getText());
                miej = 3;
            }
            if(!szukanySettingField.getText().equals("")){
                statement.setString(miej,szukanySettingField.getText());
            }

            ResultSet rs = statement.executeQuery();


            tableFilling(rs);

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    private void tableFilling(ResultSet rs) throws SQLException {

        wolneKampanies = FXCollections.observableArrayList();

        tabelka.getColumns().clear();

        for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {

            // We are using non property style for making dynamic table
            if (i > 0 && i < 4) {
                String columnName = rs.getMetaData().getColumnName(i + 1);
                TableColumn<WolneKampanie, String> col = new TableColumn<>(columnName.substring(0, 1).toUpperCase()+columnName.replace('_',' ').substring(1));
                col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<WolneKampanie, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<WolneKampanie, String> param) {
                        WolneKampanie kamp = param.getValue();
                        String cellData = kamp.getValue(columnName);
                        return new SimpleStringProperty(cellData);
                    }
                });
                tabelka.getColumns().add(col);
                System.out.println("Column [" + i + "] ");
            } else if (i >3) {
                String columnName = rs.getMetaData().getColumnName(i + 1);
                TableColumn<WolneKampanie, Integer> col2 = new TableColumn<>(columnName.substring(0, 1).toUpperCase()+columnName.replace('_',' ').substring(1));
                col2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<WolneKampanie, Integer>, ObservableValue<Integer>>() {
                    public ObservableValue<Integer> call(TableColumn.CellDataFeatures<WolneKampanie, Integer> param) {
                        WolneKampanie kamp = param.getValue();
                        Integer cellData = kamp.getValueInt(columnName);
                        return new SimpleIntegerProperty(cellData).asObject();
                    }
                });
                tabelka.getColumns().add(col2);
                System.out.println("Column [" + i + "] ");
            }
        }

        TableColumn<WolneKampanie, Boolean> col_action = new TableColumn<>("Szczegoly");
        col_action.setSortable(false);
        col_action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<WolneKampanie, Boolean>, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<WolneKampanie, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });
        col_action.setCellFactory(new Callback<TableColumn<WolneKampanie, Boolean>, TableCell<WolneKampanie, Boolean>>() {
            @Override
            public TableCell<WolneKampanie, Boolean> call(TableColumn<WolneKampanie, Boolean> p) {
                return new ButtonCell(tabelka);
            }
        });
        tabelka.getColumns().add(col_action);

        /********************************
         * Data added to ObservableList *
         ********************************/
        while (rs.next()) {
            WolneKampanie wolneKampanie = new WolneKampanie();

            System.out.println("O");

            wolneKampanie.setId(rs.getInt("id"));
            wolneKampanie.setTytul(rs.getString("tytul"));
            wolneKampanie.setSetting(rs.getString("setting"));
            wolneKampanie.setProwadzacy(rs.getString("prowadzacy"));
            wolneKampanie.setMax_graczy(rs.getInt("max_graczy"));
            wolneKampanie.setWolne_miejsca(rs.getInt("wolne_miejsca"));
            wolneKampanies.add(wolneKampanie);
        }
        tabelka.getItems().clear();
        // FINALLY ADDED TO TableView
        tabelka.setItems(wolneKampanies);

    }

    private void openDetail(int id) throws IOException, SQLException {

        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoSzczegolowKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaSzczegolowKampanii controler = loader.getController();
        controler.setLogin(login);
        controler.setid(id);
        controler.setConnectionUrl(connectionUrl);
        controler.Start();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();

    }

    private class ButtonCell extends TableCell<WolneKampanie, Boolean> {
        final Button cellButton = new Button("Szczegoly");

        ButtonCell(final TableView<WolneKampanie> tblView) {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        openDetail(wolneKampanies.get(getIndex()).getId());
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }
}
