import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Kampania {
    private IntegerProperty id;
    private StringProperty tytul;
    private StringProperty setting;
    private IntegerProperty max_graczy;
    private StringProperty opis;

    public Kampania() {
        id = new SimpleIntegerProperty();
        tytul = new SimpleStringProperty();
        setting = new SimpleStringProperty();
        max_graczy = new SimpleIntegerProperty();
        opis = new SimpleStringProperty();
    }

    public Kampania(IntegerProperty id, StringProperty tytul, StringProperty setting, IntegerProperty max_graczy, StringProperty opis) {
        this.id = id;
        this.tytul = tytul;
        this.setting = setting;
        this.max_graczy = max_graczy;
        this.opis = opis;
    }

    public Kampania(StringProperty tytul, StringProperty setting, IntegerProperty max_graczy, StringProperty opis) {
        this.tytul = tytul;
        this.setting = setting;
        this.max_graczy = max_graczy;
        this.opis = opis;
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getTytul() {
        return tytul.get();
    }

    public StringProperty tytulProperty() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul.set(tytul);
    }

    public String getSetting() {
        return setting.get();
    }

    public StringProperty settingProperty() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting.set(setting);
    }

    public int getMax_graczy() {
        return max_graczy.get();
    }

    public IntegerProperty max_graczyProperty() {
        return max_graczy;
    }

    public void setMax_graczy(int max_graczy) {
        this.max_graczy.set(max_graczy);
    }

    public String getOpis() {
        return opis.get();
    }

    public StringProperty opisProperty() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis.set(opis);
    }
}
