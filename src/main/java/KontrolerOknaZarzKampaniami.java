import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;
import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna sluzacego do wyswietlania widoku zapasy w przygotowanej tabeli
 */
public class KontrolerOknaZarzKampaniami {

    @FXML
    private Button Wroc;

    @FXML
    private TableView<KampaniaMin> tabelka;

    private ObservableList<KampaniaMin> kampaniaMins;

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    /**
     * @param event
     * @throws IOException Ponowne otworzenie okna wyboru funkcji i zamkniecie otwartego okna
     */
    @FXML
    void Powrot(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoMenuMistrza.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaMenuMistrza controler = loader.getController();
        controler.setLogin(login);
        controler.setConnectionUrl(connectionUrl);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }

    /**
     * @throws SQLException Metoda ta pozwala na nawiazanie polaczenia, wywolacnie select all na widoku zapasy i wyswietlenie jego zawartosci w tabeli
     *                      W funkcji tej najpierw wszystkie kolumnt tabeli sa przygotowane na "odbior danych" wszystkie dane zawarte w widoku sa przetwarzane
     *                      tak, aby mozna je bylo zobrazowac w tabeli
     */
    @FXML
    public void wypelnij() throws SQLException {
        String zap = "Select * from moje_kampanie where prowadzacy = " + "'" + login + "'";
        System.out.println(zap);
        kampaniaMins = FXCollections.observableArrayList();

        try {
            Connection conn = DriverManager.getConnection(connectionUrl);
            Statement statement = conn.prepareStatement(zap);
            ResultSet rs = statement.executeQuery(zap);
            System.out.println();
            System.out.println(zap);
            System.out.println();
            System.out.println();
            System.out.println(zap);
            System.out.println();

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {

                // We are using non property style for making dynamic table
                if (i > 0 && i < 3) {
                    String columnName = rs.getMetaData().getColumnName(i + 1);
                    TableColumn<KampaniaMin, String> col = new TableColumn<>(columnName);
                    col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, String>, ObservableValue<String>>() {
                        public ObservableValue<String> call(TableColumn.CellDataFeatures<KampaniaMin, String> param) {
                            KampaniaMin kamp = param.getValue();
                            String cellData = kamp.getValue(columnName);
                            return new SimpleStringProperty(cellData);
                        }
                    });
                    tabelka.getColumns().add(col);
                    System.out.println("Column [" + i + "] ");
                } else if (i == 4) {
                    String columnName = rs.getMetaData().getColumnName(i + 1);
                    TableColumn<KampaniaMin, Integer> col2 = new TableColumn<>(columnName);
                    col2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, Integer>, ObservableValue<Integer>>() {
                        public ObservableValue<Integer> call(TableColumn.CellDataFeatures<KampaniaMin, Integer> param) {
                            KampaniaMin kamp = param.getValue();
                            Integer cellData = kamp.getValueInt(columnName);
                            System.out.println(cellData);
                            return new SimpleIntegerProperty(cellData).asObject();
                        }
                    });
                    tabelka.getColumns().add(col2);
                    System.out.println("Column [" + i + "] ");
                }
            }

            TableColumn<KampaniaMin, Boolean> col_action = new TableColumn<>("Lista Graczy");
            col_action.setSortable(false);
            col_action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<KampaniaMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_action.setCellFactory(new Callback<TableColumn<KampaniaMin, Boolean>, TableCell<KampaniaMin, Boolean>>() {
                @Override
                public TableCell<KampaniaMin, Boolean> call(TableColumn<KampaniaMin, Boolean> p) {
                    return new ButtonCell2(tabelka);
                }
            });
            tabelka.getColumns().add(col_action);

            TableColumn<KampaniaMin, Boolean> col_action2 = new TableColumn<>("Edycja");
            col_action2.setSortable(false);
            col_action2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<KampaniaMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_action2.setCellFactory(new Callback<TableColumn<KampaniaMin, Boolean>, TableCell<KampaniaMin, Boolean>>() {
                @Override
                public TableCell<KampaniaMin, Boolean> call(TableColumn<KampaniaMin, Boolean> p) {
                    return new ButtonCell(tabelka);
                }
            });
            tabelka.getColumns().add(col_action2);
//
            TableColumn<KampaniaMin, Boolean> col_Delete = new TableColumn<>("Usuwanie");
            col_Delete.setSortable(false);
            col_Delete.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<KampaniaMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_Delete.setCellFactory(new Callback<TableColumn<KampaniaMin, Boolean>, TableCell<KampaniaMin, Boolean>>() {
                @Override
                public TableCell<KampaniaMin, Boolean> call(TableColumn<KampaniaMin, Boolean> p) {
                    return new ButtonDelete(tabelka);
                }
            });
            tabelka.getColumns().add(col_Delete);

            TableColumn<KampaniaMin, Boolean> col_Zabij = new TableColumn<>("Zabij");
            col_Zabij.setSortable(false);
            col_Zabij.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<KampaniaMin, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<KampaniaMin, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            });
            col_Zabij.setCellFactory(new Callback<TableColumn<KampaniaMin, Boolean>, TableCell<KampaniaMin, Boolean>>() {
                @Override
                public TableCell<KampaniaMin, Boolean> call(TableColumn<KampaniaMin, Boolean> p) {
                    return new ButtonCellZabij(tabelka);
                }
            });
            tabelka.getColumns().add(col_Zabij);
            /********************************
             * Data added to ObservableList *
             ********************************/
            while (rs.next()) {
                KampaniaMin kampaniaMin = new KampaniaMin();

                kampaniaMin.setId(rs.getInt("id"));
                kampaniaMin.setTytul(rs.getString("tytul"));
                kampaniaMin.setSetting(rs.getString("setting"));
                kampaniaMin.setLiczba_graczy(rs.getInt("liczba_graczy"));
                kampaniaMin.setMax_graczy(rs.getInt("max_graczy"));
                kampaniaMins.add(kampaniaMin);
//                    // Iterate Row
//                    for(int i = 1; i <= rs.getMetaData().getColumnCount(); i++){
//                        // Iterate Column
//                        String columnName = rs.getMetaData().getColumnName(i);
//                        String columnData = rs.getString(i);
//                        book.setValue(columnName, columnData);
//                    }
//                    System.out.println("Row [1] added " + book.getName());
//                    bookData.add(book);

            }
            // FINALLY ADDED TO TableView
            tabelka.setItems(kampaniaMins);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }


    private void listujGraczy(int id) throws IOException, SQLException {

        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoGraczyKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KotrolerOknaGraczyKampanii controler = loader.getController();
        controler.setConnectionUrl(connectionUrl);
        controler.setLogin(login);
        controler.setId(id);
        controler.wypelnij();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();

    }

    private void editKampania(int id) throws IOException, SQLException {

        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoEdycjiKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaEdycjiKampanii controler = loader.getController();
        controler.setLogin(login);
        controler.setid(id);
        controler.setConnectionUrl(connectionUrl);
        controler.Start();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();
        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();

    }

    private void usunKampanie(int id) throws SQLException {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement statement = conn.prepareStatement("delete from kampania where id = ?");
            statement.setInt(1, id);
            int result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        conn.close();
    }

    private void zabijPostacie(int id){
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionUrl);
            PreparedStatement statement = conn.prepareStatement("call killCharacters(?)");
            statement.setInt(1, id);
            int result = statement.executeUpdate();

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Define the button cell
    private class ButtonCell extends TableCell<KampaniaMin, Boolean> {
        final Button cellButton = new Button("Edytuj");

        ButtonCell(final TableView<KampaniaMin> tblView) {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        editKampania(kampaniaMins.get(getIndex()).getId());
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }

    // Define the button cell
    private class ButtonCellZabij extends TableCell<KampaniaMin, Boolean> {
        final Button cellButton = new Button("Zabij");

        ButtonCellZabij(final TableView<KampaniaMin> tblView) {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        zabijPostacie(kampaniaMins.get(getIndex()).getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }

    private class ButtonCell2 extends TableCell<KampaniaMin, Boolean> {
        final Button cellButton = new Button("Listuj");

        ButtonCell2(final TableView<KampaniaMin> tblView) {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        listujGraczy(kampaniaMins.get(getIndex()).getId());
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }

    private class ButtonDelete extends TableCell<KampaniaMin, Boolean> {
        final Button delButton = new Button("Usun");

        ButtonDelete(final TableView<KampaniaMin> tblView) {
            delButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    try {
                        usunKampanie(kampaniaMins.get(getIndex()).getId());
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    kampaniaMins.remove(getIndex());
                }
            });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(delButton);
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }
}
