import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;

/**
 * Kontroler okna pozwalajacego na wybor id probki do transfuzji (usuniecia z tabeli jednostek)
 */
public class KontrolerOknaEdycjiPostacii2 {

    @FXML
    private Button Wroc;

    @FXML
    private Button nowyBohater;

    @FXML
    private TextField imieField;

    @FXML
    private TextField nazwiskoField;

    @FXML
    private TextField klasaField;

    @FXML
    private TextField rasaField;

    @FXML
    private TextField silaField;

    @FXML
    private TextField zrecznoscField;

    @FXML
    private TextField kondycjaField;

    @FXML
    private TextField inteligencjaField;

    @FXML
    private TextField madroscField;

    @FXML
    private TextField charyzmaField;

    @FXML
    private TextField wiekField;

    @FXML
    private TextField wagaField;

    @FXML
    private TextField wzrostField;

    @FXML
    private TextField poziomField;

    @FXML
    private TextField pancerzField;

    @FXML
    private MenuButton plecSplit;

    @FXML
    private MenuButton kampaniaSplit;

    @FXML
    private TextField characterField;

    @FXML
    private Label blad;

    private String plecV;

    private String login;

    private Integer kampaniaId;

    private Integer idBohatera;

    public Integer getIdBohatera() {
        return idBohatera;
    }

    public void setIdBohatera(Integer idBohatera) {
        this.idBohatera = idBohatera;
    }

    private Integer id;

    private Integer idGracza;

    private Integer idKampanii;

    public Integer getId() {
        return id;
    }

    public Integer getIdGracza() {
        return idGracza;
    }

    public void setIdGracza(Integer idGracza) {
        this.idGracza = idGracza;
    }

    public Integer getIdKampanii() {
        return idKampanii;
    }

    public void setIdKampanii(Integer idKampanii) {
        this.idKampanii = idKampanii;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String connectionUrl;

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }

    /**
     * @param event
     * metoda wywolujaca polaczenie z baza dzieki URLowi z poprzednich kontrolerow, a nastepnie usuwajaca rejestr z tabeli jednostki na podstawie id wpisanego do textfield
     */
    @FXML
    void edytujPostac(ActionEvent event) throws SQLException {

        String imie = imieField.getText();
        String nazwisko = nazwiskoField.getText();
        String rasa = rasaField.getText();
        String klasa = klasaField.getText();
        String character = characterField.getText();
        Integer sila = Integer.parseInt(silaField.getText());
        Integer zrecznosc = Integer.parseInt(zrecznoscField.getText());
        Integer kondycja = Integer.parseInt(kondycjaField.getText());
        Integer inteligencja = Integer.parseInt(inteligencjaField.getText());
        Integer madrosc = Integer.parseInt(madroscField.getText());
        Integer charyzma = Integer.parseInt(charyzmaField.getText());
        Integer poziom = Integer.parseInt(poziomField.getText());
        Integer klasa_pancerza = Integer.parseInt(pancerzField.getText());
        Integer waga = Integer.parseInt(wagaField.getText());
        Integer wzrost = Integer.parseInt(wzrostField.getText());
        Integer wiek = Integer.parseInt(wiekField.getText());

        Connection conn = null;

        try{
            conn = DriverManager.getConnection(connectionUrl);
                PreparedStatement statement;
                statement = conn.prepareStatement("update bohater set imie = ?,nazwisko_pseudonim = ?,rasa = ?,klasa = ?,poziom = ?,sila = ?,zrecznosc = ?,kondycja = ?,inteligencja = ?,madrosc = ?,charyzma = ?,plec = ?,charakter = ?,waga = ?,wiek = ?,wzrost = ?,klasa_pancerza = ?,kampania_id = ? where id = ?");

            statement.setString(1,imie);
            statement.setString(2,nazwisko);
            statement.setString(3,rasa);
            statement.setString(4,klasa);
            statement.setInt(5,poziom);
            statement.setInt(6,sila);
            statement.setInt(7,zrecznosc);
            statement.setInt(8,kondycja);
            statement.setInt(9,inteligencja);
            statement.setInt(10,madrosc);
            statement.setInt(11,charyzma);
            statement.setString(12,plecV);
            statement.setString(13,character);
            statement.setInt(14,waga);
            statement.setInt(15,wiek);
            statement.setInt(16,wzrost);
            statement.setInt(17,klasa_pancerza);
            statement.setInt(18,kampaniaId);
            statement.setInt(19,id);



            int result = statement.executeUpdate();
            if(result!=1) {
                blad.setText("Cos poszlo nie tak");
            } else {
                blad.setText("Poprawnie");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        conn.close();
    }

    /**
     * @param event
     * @throws IOException
     * Powrot do okna pozwalajacego na wybor funkcji
     */
        @FXML
    void Powrot(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader();
        URL fxmlUrl = getClass().getClassLoader().getResource("fxml/OknoBohaterowGraczaKampanii.fxml");
        loader.setLocation(fxmlUrl);
        loader.load();
        KontrolerOknaBohaterowGraczaKampanii controler = loader.getController();
        controler.setConnectionUrl(connectionUrl);
        controler.setLogin(login);
        controler.setId(id);
        controler.setIdGracza(idGracza);
        controler.wypelnij();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.setTitle("ProjektBDLog");
        primaryStage.show();

        Stage stageteraz = (Stage) Wroc.getScene().getWindow();
        stageteraz.close();
    }


    public void start() {
            MenuItem menuItemF = new MenuItem("F");
            MenuItem menuItemM = new MenuItem("M");
            menuItemF.setOnAction(a->
                    {plecV = "F";
                    plecSplit.setText("F");
                    }
                    );
        menuItemM.setOnAction(a->
                {plecV = "M";
                plecSplit.setText("M");
                }
        );

            plecSplit.getItems().addAll(menuItemF,menuItemM);

            MenuItem menuItemZ = new MenuItem("Zadna");
            menuItemZ.setOnAction(a->{
                kampaniaId=null;
                kampaniaSplit.setText("Nie przypisana");
            });
            kampaniaSplit.getItems().add(menuItemZ);

        Connection conn = null;

        try{
            conn = DriverManager.getConnection(connectionUrl);
            PreparedStatement statement = conn.prepareStatement("select id, tytul from kampania where id in (select  kampania_id from gracz_kampania where gracz_id = ?)");
            statement.setInt(1,idGracza);

            System.out.println(login);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                 MenuItem menuItemK = new MenuItem(resultSet.getString("tytul").replace('_',' '));
                 int id = resultSet.getInt("id");
                 menuItemK.setOnAction(a->{
                     try {
                         kampaniaId = id;
                         kampaniaSplit.setText(menuItemK.getText());
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 });
                 kampaniaSplit.getItems().add(menuItemK);
            }

            statement = conn.prepareStatement("select  * from bohater where id = ? ");
            statement.setInt(1,idBohatera);
            resultSet = statement.executeQuery();
            resultSet.next();
            imieField.setText(resultSet.getString("imie"));
            nazwiskoField.setText(resultSet.getString("nazwisko_pseudonim"));
            klasaField.setText(resultSet.getString("klasa"));
            rasaField.setText(resultSet.getString("rasa"));
            silaField.setText(String.valueOf(resultSet.getInt("sila")));
            zrecznoscField.setText(String.valueOf(resultSet.getInt("zrecznosc")));
            kondycjaField.setText(String.valueOf(resultSet.getInt("kondycja")));
            inteligencjaField.setText(String.valueOf(resultSet.getInt("inteligencja")));
            madroscField.setText(String.valueOf(resultSet.getInt("madrosc")));
            charyzmaField.setText(String.valueOf(resultSet.getInt("charyzma")));
            wiekField.setText(String.valueOf(resultSet.getInt("wiek")));
            wagaField.setText(String.valueOf(resultSet.getInt("waga")));
            wzrostField.setText(String.valueOf(resultSet.getInt("wzrost")));
            poziomField.setText(String.valueOf(resultSet.getInt("poziom")));
            pancerzField.setText(String.valueOf(resultSet.getInt("klasa_pancerza")));
            plecSplit.setText(resultSet.getString("plec"));
            plecV = resultSet.getString("plec");
            kampaniaId=resultSet.getInt("kampania_id");
            characterField.setText(resultSet.getString("charakter"));

            statement = conn.prepareStatement("select tytul from kampania where id = ?");
            statement.setInt(1,kampaniaId);
            resultSet = statement.executeQuery();
            if(resultSet.next()) {
                kampaniaSplit.setText(resultSet.getString(1));
            } else {
                kampaniaSplit.setText("Zadna kampania");
            }
            conn.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
