import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class KampaniaMinMin {
    private SimpleIntegerProperty id;
    private SimpleStringProperty tytul;
    private SimpleStringProperty setting;
    private SimpleStringProperty prowadzacy;

    public KampaniaMinMin() {
        id = new SimpleIntegerProperty();
        tytul = new SimpleStringProperty();
        setting = new SimpleStringProperty();
        prowadzacy = new SimpleStringProperty();
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getTytul() {
        return tytul.get();
    }

    public SimpleStringProperty tytulProperty() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul.set(tytul);
    }

    public String getSetting() {
        return setting.get();
    }

    public SimpleStringProperty settingProperty() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting.set(setting);
    }

    public String getProwadzacy() {
        return prowadzacy.get();
    }

    public SimpleStringProperty prowadzacyProperty() {
        return prowadzacy;
    }

    public void setProwadzacy(String prowadzacy) {
        this.prowadzacy.set(prowadzacy);
    }

    public String getValue(String columnname){
        if(columnname.equalsIgnoreCase("tytul")){
            return tytul.getValue();
        } else if(columnname.equalsIgnoreCase("setting")){
            return setting.getValue();
        } else if(columnname.equalsIgnoreCase("prowadzacy")){
            return prowadzacy.getValue();
        } else {
            return null;
        }
    }
}
